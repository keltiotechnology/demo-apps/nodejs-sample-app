# Nodejs sample app

## Env vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| APP_PORT | The port to run the app | `string` | 3000 | no |
| DB_HOST | The app db host | `string` | `localhost` | no |
| DB_NAME | The app db name | `string` | `test` | no |
| DB_PORT | The app db port | `number` | `5432` | no |
| DB_USER | The app db username | `string` | `admin` | no |
| DB_PASSWORD | The app db password | `string` | `password` | no |
| REDIS_HOST | The app db name | `string` | `localhost` | no |
| REDIS_PORT | The app db port | `number` | `6379` | no |
| REDIS_USERNAME | The app db username | `string` | `admin` | no |
| REDIS_PASSWORD | The app db password | `string` | `password` | no |
