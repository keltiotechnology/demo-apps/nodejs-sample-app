.DEFAULT_GOAL := help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


##@ Docker
########################################

.PHONY: docker-login
docker-login: ## Login to docker registry
	cd devops && bash docker_login.sh

.PHONY: docker-build
docker-build: ## Build docker image
	cd devops && bash docker_build.sh "$(DOCKER_IMAGE_TAG)"

.PHONY: docker-push
docker-push: ## Push docker image
	cd devops && bash docker_push.sh "$(DOCKER_IMAGE_TAG)"

.PHONY: docker-build-push
docker-build-push: docker-build docker-push ## Build and push docker image
