#!/bin/bash
. ./colors.sh
. ./utils.sh


echo -e "${BLUE}Build docker image${NC}"
echo -e "${CYAN}Check prerequisites${NC}"
check_prerequisites_env_vars 'DOCKER_REGISTRY DOCKER_IMAGE_NAME'


echo -e "${CYAN}Set build vars${NC}"
if [ -n "$1" ]
then
  DOCKER_IMAGE_TAG=$1
else
  DOCKER_IMAGE_TAG=latest
fi
echo "DOCKER_IMAGE_TAG=$DOCKER_IMAGE_TAG"


echo -e "${CYAN}Build docker image ...${NC}"
docker_image_full_name="$DOCKER_REGISTRY/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG"
docker build -t "$docker_image_full_name" ../. && echo -e "${BGREEN}Build docker image OK : $docker_image_full_name${NC}"

# docker scan $docker_image_full_name