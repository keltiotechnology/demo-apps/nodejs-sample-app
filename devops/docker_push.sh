#!/bin/bash
. ./colors.sh
. ./utils.sh


echo -e "${BBLUE}Push docker image${NC}"
echo -e "${CYAN}Check prerequisites${NC}"
check_prerequisites_env_vars 'DOCKER_REGISTRY DOCKER_IMAGE_NAME'


echo -e "${CYAN}Set push vars${NC}"
if [ -n "$1" ]
then
  DOCKER_IMAGE_TAG=$1
else
  DOCKER_IMAGE_TAG=latest
fi
echo "DOCKER_IMAGE_TAG=$DOCKER_IMAGE_TAG"

if [[ "$DOCKER_REGISTRY_TYPE" == "AWS_ECR" ]]; then
echo "${BLACK}Notes : Make sur to add the current IAM logged user to the `repository_read_write_access_arns` config and that the ECR registry name ($DOCKER_REGISTRY/$DOCKER_IMAGE_NAME) exists${NC}"
fi

docker_image_full_name="$DOCKER_REGISTRY/$DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG"
echo -e "${CYAN}Push docker image : $docker_image_full_name ...${NC}"
docker push "$docker_image_full_name" && echo -e "${BGREEN}Docker push OK : $docker_image_full_name${NC}"
