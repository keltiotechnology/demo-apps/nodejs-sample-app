#!/bin/bash

check_prerequisites_env_vars () {
  for env_var in $1
  do
    if [[ -z "${!env_var}" ]]; then
      echo "Error : Variable $env_var not set!"
      exit 1
    else
      echo "$env_var detected : OK"
    fi
  done
}
