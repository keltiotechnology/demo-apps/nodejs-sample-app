#!/bin/bash
. ./colors.sh
. ./utils.sh


echo -e "${BLUE}Docker login${NC}"

if [[ "$DOCKER_REGISTRY_TYPE" == "BASIC" ]]; then
    echo -e "${CYAN}Check prerequisites${NC}"
    check_prerequisites_env_vars 'DOCKER_REGISTRY_USER DOCKER_REGISTRY_PASSWORD'


    echo -e "${CYAN}Docker login ...${NC}"
    docker login -u "$DOCKER_REGISTRY_USER" -p "$DOCKER_REGISTRY_PASSWORD" && echo -e "${BGREEN}Docker login OK${NC}" || echo -e "${RED}Docker login FAIL${NC}"
fi

if [[ "$DOCKER_REGISTRY_TYPE" == "AWS_ECR" ]]; then
    echo -e "${CYAN}Check prerequisites${NC}"
    check_prerequisites_env_vars 'DOCKER_REGISTRY AWS_DEFAULT_REGION AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY'


    echo -e "${CYAN}Docker login ...${NC}"
    aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $DOCKER_REGISTRY && echo -e "${BGREEN}Docker login OK${NC}" || echo -e "${RED}Docker login FAIL${NC}"
fi
