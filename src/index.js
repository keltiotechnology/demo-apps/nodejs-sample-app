const express = require("express");
const bodyParser = require('body-parser')
const app = express();
const redis = require("redis");
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

////////////////////////////////////////////////////////////////////////////////////////////
//
// SERVICES
//
////////////////////////////////////////////////////////////////////////////////////////////
const APP_PORT = process.env.APP_PORT || 3000
const { Pool } = require("pg");
const pool = new Pool({
  host: process.env.DB_HOST || 'localhost',
  database: process.env.DB_NAME || 'database',
  port: process.env.DB_PORT || 5432,
  user: process.env.DB_USER || 'admin',
  password: process.env.DB_PASSWORD || 'password'
});
const redis_host= process.env.REDIS_HOST || 'localhost' ; 
const redis_port = process.env.REDIS_PORT || 6379;
const redis_username= process.env.REDIS_USERNAME || 'admin' ;
const redis_password= process.env.REDIS_PASSWORD || 'password' ;
const redisClient = redis.createClient({
    url: 'redis://' + redis_host + ':' + redis_port,
    username: redis_username,
    password: redis_password
  });
  redisClient.on('ready', () => {
    console.log('redis is connected');
  });
  redisClient.on('error', (err) => {
    console.log('redis is disconnected: ', err);
  });
  (async () => {
    try {
      await redisClient.connect();
    } catch (error) {
      console.error('error while connecting redis', error);
    }
  })();
const initDatabase = async () => {
    const sql_create = `CREATE TABLE IF NOT EXISTS Livres (
        Livre_ID SERIAL PRIMARY KEY,
        Titre VARCHAR(100) NOT NULL,
        Auteur VARCHAR(100) NOT NULL,
        Commentaires TEXT
    );`;
    await pool.query(sql_create, [])
    console.log("Création réussie de la table 'Livres'");
    // Alimentation de la table
    const sql_insert = `INSERT INTO Livres (Livre_ID, Titre, Auteur, Commentaires) VALUES
    (1, 'Mrs. Bridge', 'Evan S. Connell', 'Premier de la série'),
    (2, 'Mr. Bridge', 'Evan S. Connell', 'Second de la série'),
    (3, 'L''ingénue libertine', 'Colette', 'Minne + Les égarements de Minne')
    ON CONFLICT DO NOTHING;`;
    await pool.query(sql_insert, [])
    const sql_sequence = "SELECT SETVAL('Livres_Livre_ID_Seq', MAX(Livre_ID)) FROM Livres;";
    await pool.query(sql_sequence, [])
    console.log("Alimentation réussie de la table 'Livres'");
}
////////////////////////////////////////////////////////////////////////////////////////////
//
// ROUTES
//
////////////////////////////////////////////////////////////////////////////////////////////

app.get('/livres', async (req, res) => {
  let isCached = false;
    try {
      const cachedData = await redisClient.get('livres');
      if (cachedData) {
        isCached = true;
        res.status(200).json({ status: 200, success: true, isCached, data: JSON.parse(cachedData) });
      } else {
        const sql = 'SELECT * FROM Livres ORDER BY Titre';
        const result = await pool.query(sql, []);
        isCached = false
        if (redisClient.ping ){
          await redisClient.set('livres', JSON.stringify(result.rows))
        }    
        res.status(200).json({ status: 200, success: true,isCached, data: result.rows });
      }
    } catch (err) {
      console.error(err);
      res.status(500).json({ status: 500, success: false, message: 'Internal server error' });
    }
  });

app.post("/livre", async (req, res) => {
    const sql = "INSERT INTO Livres (Titre, Auteur, Commentaires) VALUES ($1, $2, $3) RETURNING *";
    const book = [req.body.titre, req.body.auteur, req.body.commentaires];
    const result = await pool.query(sql, book)

    res.status(200).json({ status: 200, success: true, data: result.rows[0] })
});

app.listen(APP_PORT, async () => {
    await initDatabase();

    console.log(`Serveur démarré (http://localhost:${APP_PORT}/) !`);
});
