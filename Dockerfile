ARG NODE_VERSION=18.14.0

FROM node:$NODE_VERSION

USER node
WORKDIR /home/node

# Copy package.json and package-lock-json
COPY --chown=node:node package*.json ./

# Install dependencies
RUN npm install

COPY --chown=node:node . .

CMD ["npm", "start"]
